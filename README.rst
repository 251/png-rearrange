PNG Rearrange
=============

If you ever needed to:

- select, resort or drop chunks,

- repair crc values or file signatures of PNG images or

- create empty files ;)

PNG Rearrange might be the solution.

Usage Examples
--------------

Display help:

::

    >./png_rearrange.py -h
    PNG Rearrange.

    Selects chunks from <infile.png> and writes them rearranged by the user to
    <outfile.png>.

    Usage:
      png_rearrange.py [-md] <infile.png> <outfile.png>
      png_rearrange.py [-f] <infile.png> <outfile.png>
      png_rearrange.py -h

    Options:
      -d --drop-signature     don't write PNG signature to outfile.png
      -f --file-signature     write signature from infile.png to outfile.png
      -h --help               show help message
      -m --missing-signature  infile.png misses PNG signature

    User Input:
      Comma separated list of of chunk indices or index ranges, p. ex.
        0,2..7,1,2,8
      or 'exit'.


Repair crc and drop chunk:

::

    >./png_rearrange.py crc.png crc_repaired.png
    File: crc.png
    No. Type (Position,Length)
    --------------------------
    [Error] wrong crc value
    Repair crc [y|n]: y
     0. IHDR (8,25)
     1. gAMA (33,16)
     2. PLTE (49,780)
     3. IDAT (829,445)
     4. PLTE (1274,780)
     5. IEND (2054,12)
    Enter new order (p.ex.: 0,3..5,2): 0..3,5
    New order: [0, 1, 2, 3, 5]


Add missing file signature:

::

    >./png_rearrange.py -m nosig.png sig.png
    File: nosig.png
    No. Type (Position,Length)
    --------------------------
     0. IHDR (0,25)
     1. gAMA (25,16)
     2. PLTE (41,780)
     3. IDAT (821,445)
     4. IEND (1266,12)
    Enter new order (p.ex.: 0,3..5,2): 0..4
    New order: [0, 1, 2, 3, 4]


Error handling
--------------

PNG Rearrange is written in exception-polluted Python. For the sake of
readability and simplicity most exceptions are left uncaught. Please imagine
a fancy error message similar to the last line of the stack trace.
Temporary files (``png_rearrange_xxxx.png``) are created in the destination
directory and are purposely not deleted in these cases.


Dependencies
------------

PNG Rearrange depends on docopt_ and was tested on Python 3.2.3.

.. _docopt: https://github.com/docopt/docopt


License
-------

SimPL-2.0_

.. _SimPL-2.0: http://opensource.org/licenses/simpl-2.0

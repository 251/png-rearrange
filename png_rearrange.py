#!/usr/bin/python3

# PNG Rearrange
#
# author: Frank Busse
# license: SimPL-2.0
#
# Literature
# ----------
# [PNG] Portable Network Graphics (PNG) Specification (Second Edition)

"""PNG Rearrange.

Selects chunks from <infile.png> and writes them rearranged by the user to
<outfile.png>.

Usage:
  png_rearrange.py [-md] <infile.png> <outfile.png>
  png_rearrange.py [-f] <infile.png> <outfile.png>
  png_rearrange.py -h

Options:
  -d --drop-signature     don't write PNG signature to outfile.png
  -f --file-signature     write signature from infile.png to outfile.png
  -h --help               show help message
  -m --missing-signature  infile.png misses PNG signature

User Input:
  Comma separated list of of chunk indices or index ranges, p. ex.
    0,2..7,1,2,8
  or 'exit'.
"""

import io, os, readline, struct, sys, tempfile, zlib
from docopt import docopt
from functools import reduce


PNG_SIGNATURE = b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a'
BUFMAX = 4096


def print_error(*msgs):
	"""print highlighted error message(s) to stdout"""

	msgs = ' '.join(map(str, msgs))
	print('\033[1;31m[Error] {0}\033[1;m'.format(msgs))


def ask_yes_no(s):
	"""print s, read y|n from stdin, return True|False"""

	while True:
		c = input(s).strip().lower()
		if c == 'y': return True
		elif c == 'n': return False
		else: print_error('wrong input, just y or n')


def read(src, count):
	"""read and return count bytes from src"""

	data = src.read(count)
	if len(data) != count:
		print_error('read error')
		exit(1)

	return data


def read_and_apply(src, length, f):
	"""incrementally read from src up to length bytes and apply f repeatedly to buffer"""

	bufs, rest = divmod(length,BUFMAX)
	buf = bytearray(BUFMAX)
	mv  = memoryview(buf)
	for n in [BUFMAX]*bufs + [rest]:
		src.readinto(mv[:n])
		f(mv[:n])


def read_png_chunks(src):
	"""read chunks from src into list [{'type','pos','length'[,'crc']}]"""

	# chunk layout [PNG, 5.3]
	# +-------------------------------------------------+
	# | length (4) | type (4) | data (length) | crc (4) |
	# +-------------------------------------------------+

	ret = []
	cnt = 0

	print('No. Type (Position,Length)\n' + '-'*26)
	while True:
		# read chunk
		pos = src.tell()
		length_ = src.read(4)
		if len(length_) == 4:
			# chunk length [PNG, 5.1, 7.1]
			(chunk_length,) = struct.unpack_from('!I', length_, 0)
			if not 0 <= chunk_length < 2**31:
				print_error('illegal chunk length')
				exit(1)

			# chunk type
			type_ = read(src, 4)
			chunk_type = ''
			try:
				chunk_type = type_.decode('ascii')
			except:
				print_error('non-ascii characters in type')
				chunk_type = type_.decode('ascii', 'replace')

			# chunk data
			zcrc = zlib.crc32(type_)
			if chunk_length > 0:
				def action(buf):
					nonlocal zcrc
					zcrc = zlib.crc32(buf, zcrc)

				read_and_apply(src, chunk_length, action)

			# chunk crc
			crc_   = read(src, 4)
			(chunk_crc,) = struct.unpack_from('!I', crc_, 0)

			chunk_attrs = {'type':chunk_type, 'pos':pos, 'length':chunk_length+12}

			# check crc and repair
			if chunk_crc != zcrc:
				print_error('wrong crc value')
				if ask_yes_no('Repair crc [y|n]: '):
					chunk_attrs['crc'] = struct.pack('!I', zcrc)

			ret.append(chunk_attrs)
			print('{3:>2}. {0} ({1},{2})'.format(chunk_type, pos, chunk_length+12, cnt))
			cnt += 1

		# eof
		elif not length_: break
		# error
		else:
			print_error('defective chunk length')
			break

	return ret


def order_from_string(s, min, max):
	"""create list of indices in range [min,max-1] from string s

	example:
	>>> order_from_string("0,5..2,3,4..7",0,7)
	[Error] indices out of range: [7]
	>>> order_from_string("0,5..2,3,4..7",0,8)
	[0, 5, 4, 3, 2, 3, 4, 5, 6, 7]
	"""

	err = False

	def convert(si):
		nonlocal err

		if not si: return []

		try:
			# simple integer conversion
			i = int(si)
		except:
			# range notation
			try:
				a, b = si.split('..')
				c, d = int(a), int(b)
				l   = []
				if c < d:
					l = list(range(c, d+1))
				else:
					l = list(range(d, c+1))
					l.reverse()
			except:
				print_error('unknown representation:', si)
				err = True
				return []
			else: return l
		else: return [i]


	# split comma separated list, convert ranges into lists of indices, join lists
	order = reduce(lambda x, i: x + convert(i.strip()), s.split(','), [])

	# all indices in range [min..max-1]?
	l = [i for i in order if not min <= i < max]
	if l:
		print_error('indices out of range:', l);
		err = True

	return order if not err else None


def copy_chunk(src, dst, chunk):
	"""copy chunk from src to dst"""

	# seek
	src.seek(chunk['pos'], io.SEEK_SET)

	#copy
	written = 0
	length = chunk['length'] if 'crc' not in chunk else chunk['length']-4

	def action(buf):
		nonlocal written
		written += dst.write(buf)

	read_and_apply(src, length, action)

	# append corrected crc
	if 'crc' in chunk:
		written += dst.write(chunk['crc'])

	return written == chunk['length']



if __name__ == '__main__':
	# parse command line arguments
	arguments = docopt(__doc__, argv=sys.argv[1:], help=True, version=None)

	# read <infile.png>
	print('\033[1;32mFile: {0}\033[1;m'.format(arguments['<infile.png>']))
	infile = open(arguments['<infile.png>'], 'rb')

	# - read signature [PNG, 5.2]
	if not arguments['--missing-signature']:
		infile_signature = read(infile, 8)
		if infile_signature != PNG_SIGNATURE:
			print_error('invalid PNG signature')

	# - read chunks
	chunks = read_png_chunks(infile)


	# ask for new order
	while True:
		cmd = input('Enter new order (p.ex.: 0,3..5,2): ').strip()
		if cmd == 'exit': exit(0)

		order = order_from_string(cmd, 0, len(chunks))
		if order is not None:
			print('New order: {0}'.format(order))
			break


	# write <outfile.png>
	# - create temporary file in /path/ from <outfile.png>
	path = os.path.abspath(arguments['<outfile.png>'])
	directory = os.path.dirname(path)
	tmpfile_, tmpname = tempfile.mkstemp(prefix='png_rearrange_', suffix='.png', dir=directory)
	tmpfile = os.fdopen(tmpfile_, 'wb')

	# - write data
	# -- write signature
	if not arguments['--drop-signature']:
		tmpfile.write(infile_signature if arguments['--file-signature'] else PNG_SIGNATURE)

	# -- write chunks
	for n in order:
		chunk = chunks[n]
		if not copy_chunk(infile, tmpfile, chunk):
			print_error('copy error')
			exit(1)

	# - close files, rename tmpfile to <outfile.png>
	infile.close()
	tmpfile.close()
	if not os.path.isfile(path) or ask_yes_no('Overwrite ' + path + ' [y|n]: '): # race condition
		os.rename(tmpname, path)
